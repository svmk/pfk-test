<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PharmacyAlias]].
 *
 * @see PharmacyAlias
 */
class PharmacyAliasQuery extends \yii\db\ActiveQuery
{
    
    public function thatHaveAddress($address) {
        return $this->andWhere(['address' => trim($address)]);
    }
}
