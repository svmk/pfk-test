<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Product]].
 *
 * @see Product
 */
class ProductQuery extends \yii\db\ActiveQuery
{
    public function thatHaveName($name) {
        return $this->andWhere(['name' => trim($name)]);
    } 
}
