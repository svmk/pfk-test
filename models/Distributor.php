<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distributor".
 *
 * @property integer $id
 * @property string $name
 *
 * @property DistributorAlias[] $distributorAliases
 * @property ProductIncoming[] $productIncomings
 */
class Distributor extends \app\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distributor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',],'required'],
            [['name',], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributorAliases()
    {
        return $this->hasMany(DistributorAlias::className(), ['distributor_id' => 'id'])->inverseOf('distributor');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductIncomings()
    {
        return $this->hasMany(ProductIncoming::className(), ['distributor_id' => 'id'])->inverseOf('distributor');
    }

    /**
     * @inheritdoc
     * @return DistributorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DistributorQuery(get_called_class());
    }
}
