<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Pharmacy]].
 *
 * @see Pharmacy
 */
class PharmacyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function thatHaveAddress($address) {
        return $this->andWhere(['address' => trim($address)]);
    }
}
