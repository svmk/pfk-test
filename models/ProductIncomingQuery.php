<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ProductIncoming]].
 *
 * @see ProductIncoming
 */
class ProductIncomingQuery extends \yii\db\ActiveQuery
{
    public function thathaveIds($ids) {
        return $this->andWhere(['id' => $ids,]);
    }

    public function groupByDistributor() {
    	return $this->groupBy('distributor_id');
    }

    public function sumQuantity() {
    	return $this->sum('quantity');
    }

    public function selectDistributorAndSumQuantity() {
    	return $this->select('distributor_id,sum(quantity) as sumQuantity');
    }
}
