<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pharmacy".
 *
 * @property integer $id
 * @property string $address
 *
 * @property ProductIncoming[] $productIncomings
 */
class Pharmacy extends \app\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pharmacy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address',],'required'],
            [['address',], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Адрес',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPharmacyAlias()
    {
        return $this->hasMany(PharmacyAlias::className(), ['pharmacy_id' => 'id'])->inverseOf('pharmacy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductIncomings()
    {
        return $this->hasMany(ProductIncoming::className(), ['pharmacy_id' => 'id'])->inverseOf('pharmacy');
    }

    /**
     * @inheritdoc
     * @return PharmacyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PharmacyQuery(get_called_class());
    }

    /**
     * getTitle возвращает название сервера
     * 
     * @return string
     */
    public function getTitle() {
        return $this->address;
    }
}
