<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 *
 * @property DistributorAlias[] $distributorAliases
 * @property ProductIncoming[] $productIncomings
 */
class Product extends \app\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',],'required'],
            [['name',], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название товара',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributorAliases()
    {
        return $this->hasMany(DistributorAlias::className(), ['product_id' => 'id'])->inverseOf('product');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductIncomings()
    {
        return $this->hasMany(ProductIncoming::className(), ['product_id' => 'id'])->inverseOf('product');
    }

    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
}
