<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distributor_alias".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $distributor_id
 * @property string $name
 *
 * @property Distributor $distributor
 * @property Product $product
 */
class DistributorAlias extends \app\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distributor_alias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'distributor_id', 'name'], 'required'],
            [['product_id', 'distributor_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['distributor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Distributor::className(), 'targetAttribute' => ['distributor_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Продукт',
            'distributor_id' => 'Дистрибьютор',
            'name' => 'Синоним',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributor()
    {
        return $this->hasOne(Distributor::className(), ['id' => 'distributor_id'])->inverseOf('distributorAliases');
    }

    /**
     * @param app\models\Distributor $model модель дистрибьютора
     */
    public function setDistributor(Distributor $model)
    {
        $this->distributor_id = $model->getPrimaryKey();   
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->inverseOf('distributorAliases');
    }

    /**
     * @param app\models\Product $model модель товара
     */
    public function setProduct(Product $model)
    {
        $this->product_id = $model->getPrimaryKey();
    }

    /**
     * @inheritdoc
     * @return DistributorAliasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DistributorAliasQuery(get_called_class());
    }
}
