<?php

namespace app\models;

use Yii;
use yii\validators\DateValidator;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_incoming".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $distributor_id
 * @property integer $pharmacy_id
 * @property integer $quantity
 *
 * @property Distributor $distributor
 * @property Pharmacy $pharmacy
 * @property Product $product
 */
class ProductIncoming extends \app\models\ActiveRecord
{

    public $sumQuantity;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_incoming';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'distributor_id', 'pharmacy_id', 'quantity'], 'integer'],
            [['date'],'date','type' => DateValidator::TYPE_DATETIME,],
            [['distributor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Distributor::className(), 'targetAttribute' => ['distributor_id' => 'id']],
            [['pharmacy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pharmacy::className(), 'targetAttribute' => ['pharmacy_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'product_id' => 'Товар',
            'distributor_id' => 'Дистрибьютор',
            'pharmacy_id' => 'Аптека',
            'quantity' => 'Количество',
            'date' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributor()
    {
        return $this->hasOne(Distributor::className(), ['id' => 'distributor_id'])->inverseOf('productIncomings');
    }

    /**
     * @param app\models\Distributor $model модель дистрибьютора
     */
    public function setDistributor(Distributor $model)
    {
        $this->distributor_id = $model->getPrimaryKey();   
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPharmacy()
    {
        return $this->hasOne(Pharmacy::className(), ['id' => 'pharmacy_id'])->inverseOf('productIncomings');
    }

    /**
     * @param app\models\Pharmacy $model модель аптеки
     */
    public function setPharmacy(Pharmacy $model)
    {
        $this->pharmacy_id = $model->getPrimaryKey(); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->inverseOf('productIncomings');
    }        

    /**
     * @param app\models\Product $model модель товара
     */
    public function setProduct(Product $model)
    {
        $this->product_id = $model->getPrimaryKey();
    }

    /**
     * @inheritdoc
     * @return ProductIncomingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductIncomingQuery(get_called_class());
    }
}
