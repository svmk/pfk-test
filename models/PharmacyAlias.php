<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pharmacy_alias".
 *
 * @property integer $id
 * @property integer $pharmacy_id
 * @property string $address
 *
 * @property Pharmacy $pharmacy
 */
class PharmacyAlias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pharmacy_alias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pharmacy_id','address',],'required',],
            [['pharmacy_id'], 'integer'],
            [['address'], 'string', 'max' => 255],
            [['pharmacy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pharmacy::className(), 'targetAttribute' => ['pharmacy_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pharmacy_id' => 'Pharmacy ID',
            'address' => 'Адрес',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPharmacy()
    {
        return $this->hasOne(Pharmacy::className(), ['id' => 'pharmacy_id'])->inverseOf('pharmacyAliases');
    }

    /**
     * @param app\models\Pharmacy $model модель аптеки
     */
    public function setPharmacy(Pharmacy $model)
    {
        $this->pharmacy_id = $model->getPrimaryKey(); 
    }

    /**
     * @inheritdoc
     * @return PharmacyAliasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PharmacyAliasQuery(get_called_class());
    }
}
