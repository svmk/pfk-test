<?php
namespace app\models;
use yii\base\Model;
use app\models\Distributor;
use app\importers\DistributorImporterCsv;
class DistributorImportForm extends Model {
    /**
     * $csvFile csv файл
     * @var yii\web\UploadedFile;
     */
	public $csvFile;

    /**
     * $column1 столбец 1
     * @var integer
     */
    public $column1;

    /**
     * $column1 столбец 2
     * @var integer
     */
    public $column2;

    /**
     * $column1 столбец 3
     * @var integer
     */
    public $column3;

    /**
     * $skipFirstLine пропускать первую строку
     * @var boolean
     */
    public $skipFirstLine;

    /**
     * columns возвращает столбцы
     * 
     * @return array
     */
    public static function columns() {
        return [
            'pharmacy' => 'Аптека',
            'product'  => 'Товар',
            'quantity' => 'Количество',
        ];
    }

    /**
     * $distributor дистрибьютор
     * @var app\models\Distributor
     */
    protected $distributor;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
             [['csvFile'], 'file', 'skipOnEmpty' => false, 'extensions' => ['csv','txt'],],
             [['column1','column2','column3',],'required'],
             [['column1','column2',],'compare','compareAttribute' => 'column3','operator' => '!=',],
             [['column2','column3',],'compare','compareAttribute' => 'column1','operator' => '!=',],
             [['column3','column1',],'compare','compareAttribute' => 'column2','operator' => '!=',],
             [['skipFirstLine',],'boolean',],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
    	return [
    		'csvFile' => 'CSV-файл',
            'column1' => 'Столбец 1',
            'column2' => 'Столбец 2',
            'column3' => 'Столбец 3',
            'skipFirstLine' => 'Пропустить первую строчку',
		];
    }

    /**
     * import импортирует данные
     * 
     * @return boolean
     */
    public function import()
    {
        $importer = new DistributorImporterCsv;
        $importer->skipFirstLine = $this->skipFirstLine;
        $columns = [
            $this->column1 => 1,
            $this->column2 => 2,
            $this->column3 => 3,     
        ];
        $importer->pharmacyColumn = $columns['pharmacy'];
        $importer->productColumn  = $columns['product'];
        $importer->quantityColumn = $columns['quantity'];
        return $importer->import(
            $this->distributor,
            $this->csvFile->tempName
        );
    }

    /**
     * __construct создаёт модель
     * 
     * @param Distributor $distributor дистрибьютор
     * @param array $config                 конфиги
     *
     */
    function __construct(Distributor $distributor,$config = []) {
    	$this->distributor = $distributor;
    	parent::__construct($config);
    }
}