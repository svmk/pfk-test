<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DistributorAlias]].
 *
 * @see DistributorAlias
 */
class DistributorAliasQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function thatHaveDistributor(Distributor $distributor) {
        return $this->andWhere([
            'distributor_id' => $distributor->getPrimaryKey(),
        ]);
    }

    public function thatHaveName($name) {
    	return $this->andWhere(['name' => trim($name)]);
    }
}
