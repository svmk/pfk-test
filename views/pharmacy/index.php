<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PharmacySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Аптеки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pharmacy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить аптеку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'address',
            [
                'class' => yii\grid\Column::className(),
                'header' => 'Количество товара',
                'content' => function($model) {
                    return $model->getProductIncomings()->sumQuantity();
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
