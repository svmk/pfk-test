<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Pharmacy */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Аптеки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pharmacy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'address',
            [
                'label' => 'Количество товара',
                'value' => $model->getProductIncomings()->sumQuantity(),
            ]
        ],
    ]) ?>

    <div class="product-distibutor-stat">        
        <h2>Список статистики по дистрибьюторам</h2>
        <?= GridView::widget([
                'dataProvider' => $distributorStatDataProvider,
                'columns' => [
                    [
                        'attribute' => 'distributor_id',
                        'value' => function($model) {
                            $distributor = $model->getDistributor()->one();
                            if ($distributor) {
                                return $distributor->name;
                            }
                        }
                    ],
                    [
                        'attribute' => 'sumQuantity',
                        'header' => 'Количество товара',
                    ],
                    // [
                    //     'class' => yii\grid\Column::className(),
                    //     'header' => 'Количество товара',
                    //     'content' => function($model) {
                    //         $model->
                    //     }
                    // ],
                ],
            ]);
        ?>
    </div>

    <div class="product-incoming-view">        
        <h2>Список поступивших препаратов</h2>
        <?= GridView::widget([
                'dataProvider' => $productIncomingDataProvider,
                'columns' => [
                    'date',
                    [
                        'attribute' => 'product_id',
                        'value' => function($model) {
                            $product = $model->getProduct()->one();
                            if ($product) {
                                return $product->name;
                            }
                        }
                    ],
                    [
                        'attribute' => 'distributor_id',
                        'value' => function($model) {
                            $distributor = $model->getDistributor()->one();
                            if ($distributor) {
                                return $distributor->name;
                            }
                        }
                    ],
                    'quantity',
                ],
            ]);
        ?>
    </div>

    <div class="pharmacy-aliases-view">        
        <h2>Список синонимов адресов</h2>
        <p>
            <a href="<?php echo Url::to(['/pharmacy-alias/create','pharmacy_id' => $model->id,]); ?>" class="btn btn-success">Добавить новый адресов</a>
        </p>
        <?= GridView::widget([
                'dataProvider' => $aliasDataProvider,
                'columns' => [
                    'address',
                    [
                        'header' => 'Управление',
                        'class' => ActionColumn::className(),
                        'template' => '{update} {delete}',
                        'controller' => 'pharmacy-alias',
                    ],
                ],
            ]);
        ?>
    </div>

</div>
