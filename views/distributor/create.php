<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Distributor */

$this->title = 'Добавить дистрибьютора';
$this->params['breadcrumbs'][] = ['label' => 'Дистрибьюторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distributor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
