<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Distributor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Дистрибьюторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view','id' => $model->id,]];
$this->params['breadcrumbs'][] = 'Импорт '.$this->title;
?>
<div class="distributor-import">

    <h1>Импорт <?= Html::encode($this->title) ?></h1>
    <div class="distributor-import-form">
        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>

        <?= $form->field($importModel, 'csvFile')->fileInput() ?>

        <?= $form->field($importModel, 'column1')->dropDownList(
            [null => 'Выберите',] + $columnData
        ) ?>
        <?= $form->field($importModel, 'column2')->dropDownList(
            [null => 'Выберите',] + $columnData
        ) ?>
        <?= $form->field($importModel, 'column3')->dropDownList(
            [null => 'Выберите',] + $columnData
        ) ?>

        <?= $form->field($importModel, 'skipFirstLine')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton('Импортировать', ['class' => 'btn btn-success',]) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
