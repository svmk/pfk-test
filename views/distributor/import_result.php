<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\grid\Column;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Distributor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Дистрибьюторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view','id' => $model->id,]];
$this->params['breadcrumbs'][] = 'Результаты импорта '.$this->title;
?>
<div class="distributor-import">

    <h1>Результаты импорта <?= Html::encode($this->title) ?></h1>
    <div class="distributor-import-errors">
        <h2>Ошибки</h2>
        <?php 
            echo GridView::widget([
                'dataProvider' => $errorDataProvider,
                'columns' => [
                    [
                        'class' => Column::className(),
                        'header' => 'Причина ошибки',
                        'content' => function ($model, $key, $index, $column) {
                            return $model->getMessage();
                        },
                    ],
                    [
                        'class' => Column::className(),
                        'header' => 'Объект ошибки',
                        'content' => function ($model, $key, $index, $column) {
                            return $model->subMessage;
                        },
                    ],                    
                ],
            ]);
        ?>
    </div>
    <div class="distributor-import-results">
        <h2>Импортировано</h2>
        <?php 
            echo GridView::widget([
                'dataProvider' => $resultDataProvider,
                'columns' => [
                    [
                        'attribute' => 'product_id',
                        'value' => function($model) {
                            $product = $model->getProduct()->one();
                            if ($product) {
                                return $product->name;
                            }
                        }
                    ],
                    [
                        'attribute' => 'pharmacy_id',
                        'value' => function($model) {
                            $pharmacy = $model->getPharmacy()->one();
                            if ($pharmacy) {
                                return $pharmacy->getTitle();
                            }
                        },
                    ],
                    'quantity',
                ],
            ]);
        ?>
    </div>
</div>
