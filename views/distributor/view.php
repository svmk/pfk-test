<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Distributor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Дистрибьюторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distributor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Импорт', ['import', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

    <div class="product-incoming-view">        
        <h2>Список поступивших препаратов</h2>
        <?= GridView::widget([
                'dataProvider' => $productIncomingDataProvider,
                'columns' => [
                    'date',
                    [
                        'attribute' => 'product_id',
                        'value' => function($model) {
                            $product = $model->getProduct()->one();
                            if ($product) {
                                return $product->name;
                            }
                        }
                    ],
                    [
                        'attribute' => 'pharmacy_id',
                        'value' => function($model) {
                            $pharmacy = $model->getPharmacy()->one();
                            if ($pharmacy) {
                                return $pharmacy->getTitle();
                            }
                        }
                    ],
                    'quantity',
                ],
            ]);
        ?>
    </div>

    <div class="distributor-aliases-view">        
        <h2>Список синонимов препаратов</h2>
        <p>
            <a href="<?php echo Url::to(['/distributor-alias/create','distributor_id' => $model->id,]); ?>" class="btn btn-success">Добавить новый синоним</a>
        </p>
        <?= GridView::widget([
                'dataProvider' => $aliasDataProvider,
                'columns' => [
                    [
                        'attribute' => 'product_id',
                        'value' => function($model) {
                            $product = $model->getProduct()->one();
                            if ($product) {
                                return $product->name;
                            }
                        }
                    ],
                    'name',
                    [
                        'header' => 'Управление',
                        'class' => ActionColumn::className(),
                        'template' => '{update} {delete}',
                        'controller' => 'distributor-alias',
                    ],
                ],
            ]);
        ?>
    </div>
</div>
