<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\pharmacy */

$this->title = 'Добавить другой адрес';
$this->params['breadcrumbs'][] = ['label' => 'Дистрибьюторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $pharmacy->getTitle();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pharmacy-alias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'productList' => $productList,
    ]) ?>

</div>
