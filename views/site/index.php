<?php

/* @var $this yii\web\View */

$this->title = 'Тестовое ПФК';
?>
<div class="site-index">
    <h1><a id="__0"></a>Тестовое задание</h1>
<h2><a id="___2"></a>Правила оформления результата</h2>
<p>Результат выполнения данного тестового задания должен быть представлен в виде репозитория
на <a href="http://github.com">GitHub</a> желательно с сохранением истории коммитов.</p>
<h2><a id="__7"></a>Используемые технологии</h2>
<ul>
<li>
<p>PHP (фреймворк или самописное что-то - не имеет значения)</p>
</li>
<li>
<p>Если используется frontend-фреймворк, то желательно AngularJS 1.x или ReactJS</p>
</li>
</ul>
<h2><a id="__13"></a>Описание задачи</h2>
<p>Необходимо реализовать систему импорта данных от дистрибьюторов продукции компании по
аптечным точкам и возможность просмотреть, сколько в каждую из аптек пришло товара суммарно
по всем дистрибьюторам аптеки.</p>
<p>Дистрибьюторы предоставляют информацию о том, в какие аптечные
точки они развезли нашу продукцию. У каждого дистрибьютора наименование наших
препаратов а также обозначение аптеки своё.</p>
<p>Файлы для импорта подготавливаются дистрибьюторами и представляют собой текстовые файлы
со строками, включающами в себя следующую информацию: аптечная точка, препарат, количество.
По одной аптеке и препарату, записи могут встречаться несколько раз.</p>
<p>Примеры файлов: <a href="distributor2.txt">Дистрибьютор 1</a>, <a href="distributor2.txt">Дистрибьютор 2</a></p>
<h2><a id="_____29"></a>Что будет делать пользователь системы</h2>
<ul>
<li>
<p>Выбирая в списке дистрибьютора, пользователь может загрузить в систему файл данных по нему.</p>
</li>
<li>
<p>Выбирая в списке аптеку, пользователь может посмотреть, сколько в аптеку пришло
товара со всех дистрибьюторов и с каждого в отдельности</p>
</li>
<li>
<p>Пользователь должен иметь возможность редактировать соответствия между наименованиями,
используемыми в файле данных и в системе (аптеки, препараты)</p>
</li>
</ul>
    
</div>
