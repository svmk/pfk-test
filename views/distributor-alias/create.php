<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Distributor */

$this->title = 'Добавить другое название';
$this->params['breadcrumbs'][] = ['label' => 'Дистрибьюторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $distributor->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distributor-alias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'productList' => $productList,
    ]) ?>

</div>
