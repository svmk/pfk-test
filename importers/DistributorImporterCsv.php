<?php
namespace app\importers;
use app\models\Distributor;
use app\models\DistributorAlias;
use app\models\Pharmacy;
use app\models\PharmacyAlias;
use app\models\Product;
use app\models\ProductIncoming;
class DistributorImporterCsv {
    /**
     * $delimiter символ разделителя
     * @var string
     */
	public $delimiter = "\t";

    /**
     * $enclosure символ ограничителя поля
     * @var string
     */
	public $enclosure = '"';

    /**
     * $escape экранирующий символ
     * @var string
     */
	public $escape = "\\";

    /**
     * $maxLength максимальная ширина
     * @var int
     */
	public $maxLength = 0;

    /**
     * $skipFirstLine пропустить первую строчку
     * @var boolean
     */
    public $skipFirstLine;

    /**
     * $pharmacyColumn столбец аптеки
     * @var integer
     */
    public $pharmacyColumn;

    /**
     * $productColumn столбец продукта
     * @var integer
     */
    public $productColumn;

    /**
     * $quantityColumn столбец количества
     * @var integer
     */
    public $quantityColumn;

    /**
     * processItem обрабатывает строку
     * 
     * @param Distributor $distributor дистрибьютор
     * @param array $data строка
     *
     * @return ProductIncoming
     */
    protected function processItem(Distributor $distributor,$data) {
        if (!isset($data[0]) || !isset($data[1]) || !isset($data[2])) {
            throw new ImportResult('Неверный формат строки');
        }
        if ($this->pharmacyColumn === null || $this->productColumn === null || $this->quantityColumn === null) {
            throw new ImportResult('Неправильные настройки импортера');
        }
        $pharmacyName= $data[$this->pharmacyColumn - 1];
        $productName = $data[$this->productColumn  - 1];
        $quantity    = $data[$this->quantityColumn - 1];
        $pharmacy = null;
        if ($pharmacy === null) {
            $pharmacyAlias = PharmacyAlias::find(
            )->thatHaveAddress(
                $pharmacyName
            )->one();
            if ($pharmacyAlias) {
                $pharmacy = $pharmacyAlias->getPharmacy()->one();
            }
        }
        if ($pharmacy === null) {
            $pharmacy = Pharmacy::find(
            )->thatHaveAddress(
                $pharmacyName
            )->one();
        }
        if ($pharmacy === null) {
            throw new ImportResult(
                'Не найдена аптека',
                $pharmacyName
            );
        }
        $product = null;
        if ($product === null) {
            $productAlias = DistributorAlias::find(
            )->thatHaveDistributor(
                $distributor
            )->thatHaveName(
                $productName
            )->one();    
            if ($productAlias) {
                $product = $productAlias->getProduct()->one();
            } 
        }
        if ($product === null) {
            $product = Product::find()->thatHaveName(
                $productName
            )->one();
        }
        if ($product === null) {
            throw new ImportResult('Не найден продукт',$productName);
        }
        $productIncoming = new ProductIncoming;
        $productIncoming->setDistributor($distributor);
        $productIncoming->setPharmacy($pharmacy);
        $productIncoming->setProduct($product);
        $productIncoming->quantity = $quantity;
        if ($productIncoming->save()) {
            return $productIncoming;
        } else {
            throw new ImportResult(
                'Не удалось сохранить запись',
                json_encode($productIncoming->getErrors())
            );
        }
    }

    /**
     * import импортирует данные
     * @param Distributor $distributor дистрибьютор
     * @param string $csvFile CSV-файл
     * @return ProductIncoming[]|ImportResult[]
     */
    public function import(Distributor $distributor,$csvFile)
    {
        $handle = fopen($csvFile, 'r');
        if ($handle) {
            $isFirst = $this->skipFirstLine;
			while (($data = fgetcsv(
				$handle, 
				$this->maxLength, 
				$this->delimiter, 
				$this->enclosure, 
				$this->escape)
			) !== false) {
                if (!$isFirst) {  
                    try {
                        yield $this->processItem($distributor,$data);
                    } catch (ImportResult $ex) {
                        yield $ex;
                    }
                }
                $isFirst = false;
			}
            fclose($handle);
		} else {
			yield new ImportResult('Не удалось открыть файл');
		}
    }
}