<?php
namespace app\importers;
use Exception;
class ImportResult extends Exception {
    /**
     * $subMessage доп. сообщение
     * @var string
     */
	public $subMessage;

    /**
     * __toString переводит исключение к строке
     * 
     * @return string
     */
	public function __toString() {
		$result = $this->message;
		if ($this->subMessage) {
			$result = $result.': '.$this->subMessage;
		}
		return $result;
	}

    /**
     * __construct конструктор исключения
     * 
     * @param string $message    сообщение
     * @param string $subMessage доп. сообщение
     *
     */
	function __construct($message,$subMessage) {
		$this->message = $message;
		$this->subMessage = $subMessage;
	}
}