Тестовое задание ПФК
=====================

Тестовое задание выполнено на базе [Yii-фреймворка](http://www.yiiframework.com/) второй версии.

Структура каталогов
-------------------
      config/parser.php                       содержит настройки парсеров
      controllers/                            содержит контроллеры
      migrations/                             содержит миграции
      models/                                 содержит модели
      importers/                              содержит импортер и исключение
      views/                                  содержит представления
      web/distributor1.txt                    тестовые данные 1
      web/distributor2.txt                    тестовые данные 2



Требования перед установкой
---------------------------

* PHP 5.4


Установка
---------

### Установите зависимости используя composer

Если у вас нет [Composer](http://getcomposer.org/), вы должны установить его [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

После чего выполните команды из корневого каталога проекта

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar install --no-dev
~~~

### Настройте подключение к БД

Отредактируйте файл `config/db.php` с настоящими настройками.

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=pfk-test',
    'username' => 'pfk-test',
    'password' => 'pfk-test',
    'charset' => 'utf8',
];
```

### Выполните миграции

Из каталога проекта выполните команду
```
./yii migrate/up
```

После выполнения всех шагов можете открыть страницу


Замечания
---------
Перед импортом аптек и дистрибьюторов нужно сделать привязки.
Некоторые привязки сделаны через миграции.
