<?php

use yii\db\Migration;

/**
 * Handles the creation for table `distributor_alias`.
 */
class m160614_034503_create_distributor_alias extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('distributor_alias', [
            'id'              => $this->primaryKey(),
            'product_id'      => $this->integer(),
            'distributor_id'  => $this->integer(),
            'name'            => $this->string(),
        ]);
        $this->createIndex(
            'distributor_alias_product_id_idx',
            'distributor_alias',
            'product_id'
        );
        $this->addForeignKey(
            'distributor_alias_product_id_fk',
            'distributor_alias',
            'product_id',
            'product',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex(
            'distributor_alias_distributor_id_idx',
            'distributor_alias',
            'distributor_id'
        );
        $this->addForeignKey(
            'distributor_alias_distributor_id_fk',
            'distributor_alias',
            'distributor_id',
            'distributor',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('distributor_alias');
    }
}
