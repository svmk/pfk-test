<?php

use yii\db\Migration;

class m160614_103057_insert_product_data extends Migration
{    
    protected function getItems() {
        return [
            ['Препарат 1',1],
            ['Препарат 2',2],
            ['Препарат 3',3],
            ['Препарат 4',4],
            ['Препарат 5',5],
            ['Препарат 6',6],
            ['Препарат 7',7],
            ['Препарат 8',8],
            ['Препарат 9',9],
            ['Препарат 10',10],
            ['Препарат 11',11],
            ['Препарат 12',12],
        ];
    }
    public function safeUp()
    {
        $items = $this->getItems();
        foreach($items as list($item,$id)) {
            $this->insert('product',['id' => $id,'name' => $item,]);
        }
    }

    public function safeDown()
    {
        $items = $this->getItems();
        foreach($items as $item) {
            $this->delete('product','name = :name',['name' => $item,]);
        }
    }
}
