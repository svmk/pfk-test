<?php

use yii\db\Migration;

/**
 * Handles the creation for table `pharmacy_alias`.
 */
class m160614_123153_create_pharmacy_alias extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pharmacy_alias', [
            'id' => $this->primaryKey(),
            'pharmacy_id' => $this->integer(),
            'address'     => $this->string(),
        ]);
        $this->createIndex(
            'pharmacy_alias_product_id_idx',
            'pharmacy_alias',
            'pharmacy_id'
        );
        $this->addForeignKey(
            'pharmacy_alias_product_id_fk',
            'pharmacy_alias',
            'pharmacy_id',
            'pharmacy',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pharmacy_alias');
    }
}
