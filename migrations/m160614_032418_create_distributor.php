<?php

use yii\db\Migration;

/**
 * Handles the creation for table `distributor`.
 */
class m160614_032418_create_distributor extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('distributor', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('distributor');
    }
}
