<?php

use yii\db\Migration;

/**
 * Handles the creation for table `product_incoming`.
 */
class m160614_032419_create_product_incoming extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_incoming', [
            'id'          => $this->primaryKey(),
            'product_id'  => $this->integer(),
            'date'        => $this->dateTime(),
            'distributor_id'  => $this->integer(),
            'pharmacy_id'     => $this->integer(),
            'quantity'    => $this->integer(),
        ]);
        $this->createIndex(
            'product_incoming_product_id_idx',
            'product_incoming',
            'product_id'
        );
        $this->addForeignKey(
            'product_incoming_product_id_fk',
            'product_incoming',
            'product_id',
            'product',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex(
            'product_incoming_distributor_id_idx',
            'product_incoming',
            'distributor_id'
        );
        $this->addForeignKey(
            'product_incoming_distributor_id_fk',
            'product_incoming',
            'distributor_id',
            'distributor',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex(
            'product_incoming_pharmacy_id_idx',
            'product_incoming',
            'pharmacy_id'
        );
        $this->addForeignKey(
            'product_incoming_pharmacy_id_fk',
            'product_incoming',
            'pharmacy_id',
            'pharmacy',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_incoming');
    }
}
