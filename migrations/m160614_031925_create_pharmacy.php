<?php

use yii\db\Migration;

/**
 * Handles the creation for table `pharmacy`.
 */
class m160614_031925_create_pharmacy extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pharmacy', [
            'id' => $this->primaryKey(),
            'address' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pharmacy');
    }
}
