<?php

use yii\db\Migration;

class m160614_124018_insert_pharmacy_alias extends Migration
{
    protected function getItems() {
        return [
            ['Маркса дом 15 стр 1', 1],
            ['Кирова улица дом 32', 2],
            ['Блюхера ул дом 4',    3],
            ['Никитина дом 73',     4],
            ['Гоголя ул дом 1',     5],
            ['Зорге ул дом 3',      6],
            ['Д-Ковальчук дом 270', 7],
        ];
    }
    public function safeUp()
    {
        $items = $this->getItems();
        foreach ($items as list($item,$id)) {
            $this->insert(
                'pharmacy_alias',
                [
                    'address' => $item,
                    'pharmacy_id' => $id,
                ]
            );
        }
    }

    public function safeDown()
    {
        $items = $this->getItems();
        foreach ($items as list($item,$id)) {
            $this->delete('pharmacy','address = :address', ['address' => $item,]);
        }
    }
}
