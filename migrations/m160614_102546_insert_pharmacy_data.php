<?php

use yii\db\Migration;

class m160614_102546_insert_pharmacy_data extends Migration
{    
    protected function getItems() {
        return [
            1 => 'пр-кт Маркса 15',
            2 => 'ул Кирова 32',
            3 => 'ул Блюхера 4',
            4 => 'ул Никитина 73',
            5 => 'ул Гоголя 1',
            6 => 'ул Зорге 3',
            7 => 'ул Дуси Ковальчук 270',
        ];
    }
    public function safeUp()
    {
        $items = $this->getItems();
        foreach ($items as $id => $item) {
            $this->insert('pharmacy',['id' => $id,'address' => $item,]);
        }
    }

    public function safeDown()
    {
        $items = $this->getItems();
        foreach ($items as $id => $item) {
            $this->delete('pharmacy','id = :id', ['id' => $id,]);
        }
    }
}
