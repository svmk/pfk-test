<?php
namespace app\controllers;

use Yii;
use app\models\Distributor;
use app\models\Product;
use app\controllers\Controller;
use app\models\DistributorAlias;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * DistributorAliasController implements the CRUD actions for Distributor model.
 */
class DistributorAliasController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * actionCreate создаёт новую запись
     * 
     * @param integer $distributor_id идентификатор поставщика
     *
     * @return string
     */
	public function actionCreate($distributor_id) {
		$distributor = Distributor::findOne($distributor_id);
		if ($distributor === null) {
			throw new NotFoundHttpException('Поставщик не найден');
		}
		$model = new DistributorAlias;
		$model->setDistributor($distributor);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
            	'/distributor/view', 
            	'id' => $distributor->id,
        	]);
        } else {        
            return $this->render('create', [
                'model' => $model,
		        'productList' => self::getProductList(),
		        'distributor' => $distributor,
            ]);
        }
	}

	/**
     * Updates an existing DistributorAlias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
    	$distributor = $model->getDistributor()->one();
    	if ($distributor === null) {
    		throw new NotFoundHttpException('Дистрибьютор не найден');
    	}

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
            	'/distributor/view', 
            	'id' => $distributor->id,
        	]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'productList' => self::getProductList(),
                'distributor' => $distributor,
            ]);
        }
    }

    /**
     * actionDelete удаляет запись
     * 
     * @param integer $id ID записи
     *
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect([
            '/distributor/view', 
            'id' => $model->distributor_id,
        ]);
    }

    /**
     * getProductList возвращает список продуктов
     * 
     * @return array
     */
    protected static function getProductList() {
    	return ArrayHelper::map(
			Product::find()->all(),
			'id',
			'name'
		);
    }
    /**
     * Finds the DistributorAlias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Distributor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DistributorAlias::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}