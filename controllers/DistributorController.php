<?php

namespace app\controllers;

use Yii;
use app\models\Distributor;
use app\models\DistributorSearch;
use app\models\DistributorImportForm;
use app\models\ProductIncoming;
use app\importers\ImportResult;
use app\controllers\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;

/**
 * DistributorController implements the CRUD actions for Distributor model.
 */
class DistributorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Distributor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DistributorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Distributor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $aliasDataProvider = new ActiveDataProvider([
            'query' => $model->getDistributorAliases(),
            'sort' => [
                'defaultOrder' => 't.date DESC',
            ],
        ]);
        $productIncomingDataProvider = new ActiveDataProvider([
            'query' => $model->getProductIncomings(),
        ]);
        return $this->render('view', [
            'aliasDataProvider' => $aliasDataProvider,
            'productIncomingDataProvider' => $productIncomingDataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Distributor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Distributor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Distributor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Distributor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * actionImport импортирует данные
     * 
     * @param integer $id id записи
     *
     * @return string
     */
    public function actionImport($id) {
        $model = $this->findModel($id);
        $importModel = new DistributorImportForm($model);
        $importModel->skipFirstLine = true;
        if (Yii::$app->request->getIsPost()) {            
            if ($importModel->load(Yii::$app->request->post())) {
                $importModel->csvFile = UploadedFile::getInstance($importModel, 'csvFile');   
                if ($importModel->validate()) {                    
                    $importedItems = $importModel->import();
                    $result = [];
                    $errors = [];
                    foreach ($importedItems as $item) {
                        if ($item instanceof ImportResult) {
                            $errors[] = $item;
                            $errors = array_unique($errors);
                        } else {
                            $result[] = $item->getPrimaryKey();
                        }
                    }
                    $resultDataProvider = new ActiveDataProvider([
                        'query' => ProductIncoming::find(
                        )->thathaveIds($result),
                    ]);
                    $errorDataProvider = new ArrayDataProvider([
                        'allModels' => $errors,
                    ]);
                    return $this->render('import_result', [
                        'resultDataProvider' => $resultDataProvider,
                        'errorDataProvider' => $errorDataProvider,
                        'importModel' => $importModel,
                        'model' => $model,
                    ]);
                }         
            }
        }
        return $this->render('import', [
            'importModel' => $importModel,
            'model' => $model,
            'columnData' => DistributorImportForm::columns(),
        ]);
    }

    /**
     * Finds the Distributor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Distributor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Distributor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
