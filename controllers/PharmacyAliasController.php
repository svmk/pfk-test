<?php
namespace app\controllers;

use Yii;
use app\models\Pharmacy;
use app\models\Product;
use app\controllers\Controller;
use app\models\PharmacyAlias;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * PharmacyAliasController implements the CRUD actions for Pharmacy model.
 */
class PharmacyAliasController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * actionCreate создаёт новую запись
     * 
     * @param integer $pharmacy_id идентификатор аптеки
     *
     * @return string
     */
	public function actionCreate($pharmacy_id) {
		$pharmacy = Pharmacy::findOne($pharmacy_id);
		if ($pharmacy === null) {
			throw new NotFoundHttpException('Аптека не найдена');
		}
		$model = new PharmacyAlias;
		$model->setPharmacy($pharmacy);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
            	'/pharmacy/view', 
            	'id' => $pharmacy->id,
        	]);
        } else {        
            return $this->render('create', [
                'model' => $model,
		        'productList' => self::getProductList(),
		        'pharmacy' => $pharmacy,
            ]);
        }
	}

	/**
     * Updates an existing PharmacyAlias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
    	$pharmacy = $model->getPharmacy()->one();
    	if ($pharmacy === null) {
    		throw new NotFoundHttpException('Дистрибьютор не найден');
    	}

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
            	'/pharmacy/view', 
            	'id' => $pharmacy->id,
        	]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'productList' => self::getProductList(),
                'pharmacy' => $pharmacy,
            ]);
        }
    }

    /**
     * actionDelete удаляет запись
     * 
     * @param integer $id ID записи
     *
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect([
            '/pharmacy/view', 
            'id' => $model->pharmacy_id,
        ]);
    }

    /**
     * getProductList возвращает список продуктов
     * 
     * @return array
     */
    protected static function getProductList() {
    	return ArrayHelper::map(
			Product::find()->all(),
			'id',
			'name'
		);
    }
    /**
     * Finds the PharmacyAlias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pharmacy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PharmacyAlias::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}