<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=pfk-test',
    'username' => 'pfk-test',
    'password' => 'pfk-test',
    'charset' => 'utf8',
];
